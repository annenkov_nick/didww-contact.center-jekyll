---
# You don't need to edit this file, it's empty on purpose.
# Edit theme's home layout instead if you wanna make some changes
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: default
title: homepage
permalink: index
#permalink: contact.center
---

  <section class="content">
    <div class="content__container">
      <div class="content__container-title">
        <h1 class="content__container-title-text">contact.center<sup class="content__container-title-text-high-index">TM</sup></h1>
        <h1 class="content__container-title-text">coming soon</h1>
      </div>
        <h2 class="content__container-text">We are working an a new exciting product that will help you to manage your contacts in a simple and clear way.</h2>
    </div>
  </section>
