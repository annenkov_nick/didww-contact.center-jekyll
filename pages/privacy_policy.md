---
title: policy
permalink: /privacy_policy
layout: default
---

  <section class="privacy">
    <div class="privacy__container">

      <h1 class="privacy__container-policy-titular-text">contact.center Privacy policy</h1>
      <p class="privacy__container-policy-text">Last updated: November 22, 2015</p>
      <p class="privacy__container-policy-text">This policy describes how DIDWW Ltd. collects and uses personal information on the contact.center™ website. By using the contact.center™ website or the contact.center™ apps, you agree to this privacy policy.</p>
      <p class="privacy__container-policy-titular-text">Information Collection and Use</p>
      <p class="privacy__container-policy-text">While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to, your name, email and phone number ("Personal Information").</p>
      <p class="privacy__container-policy-titular-text">Log Data</p>
      <p class="privacy__container-policy-text">We collect information that your browser sends whenever you visit our Service ("Log Data"). This Log Data may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages and other statistics.</p>
      <p class="privacy__container-policy-titular-text">Cookies and Pixels</p>
      <p class="privacy__container-policy-text">Cookies are small files that websites place on your computer as you browse the web. Our website uses cookies to distinguish you from other users of our website. A pixel is a small amount of code on a web page or in an email notification. We may use pixels to learn whether you’ve interacted with certain of web or email content on our Services. This helps us to provide you with a good experience when you browse our website and also allows us to improve our site.</p>
      <p class="privacy__container-policy-titular-text">Mobile Analytics</p>
      <p class="privacy__container-policy-text">We use mobile analytics software to allow us to better understand the functionality and performance of our mobile software on your phone. This software may collect, store and use information such as how often you use the application, the events that occur within the application, usage, performance data, and where the application was downloaded from. This software may share information it collects with other third parties as part of providing the analytics services and to comply with applicable laws.</p>
      <p class="privacy__container-policy-titular-text">VoIP Providers</p>
      <p class="privacy__container-policy-text">Our software is a softphone application and not a VoIP service. A SIP server or subscription with a SIP-based VoIP provider is required to use the software. For ease of use, our software may have settings preconfigured for many popular VoIP providers. We are not in any way responsible for information you have shared with or is collected by your VoIP service provider.</p>
      <p class="privacy__container-policy-titular-text">Disclosure to thirds parties</p>
      <p class="privacy__container-policy-text">We may employ third party companies and individuals to facilitate our Service, to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used. These third parties have access to your Personal Information only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.</p>
      <p class="privacy__container-policy-text">We may disclose your personal information as required by law, such as to comply with a subpoena, or similar legal process; or when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request.</p>
      <p class="privacy__container-policy-titular-text">Links to Other Sites</p>
      <p class="privacy__container-policy-text">Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>
      <p class="privacy__container-policy-text">We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>
      <p class="privacy__container-policy-titular-text">Security</p>
      <p class="privacy__container-policy-text">The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p>
      <p class="privacy__container-policy-titular-text">Data location</p>
      <p class="privacy__container-policy-text">The data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area ("EEA"). It may also be processed by staff operating outside the EEA who work for us or for one of our suppliers. Such staff maybe engaged in, among other things, the fulfilment of your order, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.</p>
      <p class="privacy__container-policy-titular-text">Changes to This Privacy Policy</p>
      <p class="privacy__container-policy-text">We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.</p>

    </div>
  </section>
